## Pilha de tecnologias
```
	* Spring Boot
	* Java 8
	* Maven
	* Sem Banco de dados
	* Este é o server back-end.
```

## Executando a aplicação no eclipse

```
1. Para rodar a aplicação no eclipse [botão direito: Run As/Java Application] em cima do arquivo
"/app-server/src/main/java/br/com/app/Application.java".
```



## Obtendo o projeto do github

	
	Página do bitbucket: https://bitbucket.org/maurecigit/number/src/develop/
	
    $ mkdir -p /workspace/app-server
    $ cd /workspace/app-server
    $ git clone https://maurecigit@bitbucket.org/maurecigit/number.git

    Ou dowload: https://bitbucket.org/maurecigit/number/get/develop.zip
	
	
## Executando no modo standalone

	$ mvn spring-boot:run
	
## Empacotando para deploy

	$ mvn clean package
	# Utilizar o arquivo target/app-server*.war 



## API's disponíveis



Operações para retornar o extenso do número inteiro enviado no path

```
HTTP : GET	-	Path : http://localhost:8090/app-server/10003

	# Retorno: dez mil e três	
```

 Os Testes são sempre executados ao compilar a aplicação através do Maven.