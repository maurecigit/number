package br.com.app.facade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

//import br.com.app.domain.Password;
//import br.com.app.domain.Tipo;
import br.com.app.service.AppService;

@RestController
@RequestMapping(value = "/", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class AppController {

	@Autowired
	private AppService appService;
	
	
	/**
	 * Return converse number to text
	 * 
	 * @return String
	 * 
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/{numbers}")
	public String findExtensive(@PathVariable int numbers) {
		
		return appService.findExtensive(numbers);
		
	}
}