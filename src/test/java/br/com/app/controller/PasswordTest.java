package br.com.app.controller;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import br.com.app.Application;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class PasswordTest {
	
	private MockMvc mock;

	private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

	@SuppressWarnings("rawtypes")
	private HttpMessageConverter mappingJackson2HttpMessageConverter;

	@Autowired
	void setConverters(HttpMessageConverter<?>[] converters) {

		this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
				.filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter).findAny().get();

		Assert.assertNotNull("the JSON message converter must not be null", this.mappingJackson2HttpMessageConverter);
	}

	
	private void expect(ResultActions teste, String password) throws Exception {
	  
	  teste.andExpect(content().contentType(contentType))	  
	  .andExpect(jsonPath("$", is(password))) ; 
	
	}
	
	/**
	 * Testar converter numero
	 * 
	 */
	@Test
	
	public void convertNumber() throws Exception {
	 
		expect(mock.perform(post("/1")).andExpect(status().isCreated()), "um");
	
	}
	
	/**
	 * Testar converter numero negativo
	 * 
	 */
	@Test
	
	public void convertNumberNegative() throws Exception {
	 
		expect(mock.perform(post("/-1")).andExpect(status().isCreated()), "menos um");
	
	}
	
	/**
	 * Testa sem parãmetro.
	 * 
	 */
	@Test
	public void nextPasswordNoExist() throws Exception {
		
		mock.perform(post("/")).andExpect(status().isNotFound());
	}
	
	@SuppressWarnings("unchecked")
	protected String json(Object o) throws IOException {
		
		MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
		this.mappingJackson2HttpMessageConverter.write(o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
		return mockHttpOutputMessage.getBodyAsString();
	}
}